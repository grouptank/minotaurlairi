void minotaurUpdate() {

  //Causes panic mode to happen
  /* After the first 3 teleports, the minotaur teleports more frequently. This is to allow the player to understand the game but also increase difficulty*/
  if (totalroundNum == 19) {

    panicMode = true;
  }

//Initial Teleports (Minotaur teleports every 6 rounds)
  if (panicMode == false) {
    if (roundNum == 6) {

      //Randomizes minotaur location in the maze
      minotaurX = int(random(1, 7));
      minotaurY = int(random(1, 7));
      
      //Resets round counter
      roundNum = 0;
    }
  }

//Panic Mode Teleports (Minotaur teleports every 3 rounds)
  if (panicMode == true) {
    if (roundNum == 3) {

      //Randomizes minotaur location in the maze
      minotaurX = int (random(1, 7));
      minotaurY = int (random(1, 7));
      
      //Resets round counter
      roundNum = 0;
    }
  }
}
