//This function displays unique text dependant on the player
void textDisplay(){
  fill(255);   
  textFont(bodyFont);
  //Displays text for the X value 1, in relation to the Y co-ordinates
  if (playerX ==1){
    if (playerY ==1){
      text ("A chilled wind blows down the hall, from where?", 30, 430);
    }else if (playerY ==2) {
      text ("You hear a whisper down the hall.", 30, 430);
    } else if (playerY ==3){
     text ("You feel breathing down your neck... but nothing is there.", 30, 430); 
    }
    else if (playerY ==4) {
      text ("A faint clinking can be heard.", 30, 430);
    }else if (playerY ==5) {
      text ("A loud shuffle is heard.You turn around. Nothing.", 30, 430);
    }else if(playerY ==6){
      text ("The smell of death clings to the walls", 30, 430);
    }else if (playerY ==7) {
      text ("A lustrous patch of moss is growing. Don't touch it!", 30, 430);
    }
  }
  
  //Displays text for the X value 2, in relation to the Y co-ordinates
  if (playerX == 2) {
    if (playerY ==1) {
      text ("Some kind of decaying animal is in the corner.", 30, 430);
    } else if (playerY ==2) {
      text ("The walls here feel moist.", 30, 430);
    }else if (playerY ==3) {
      text ("A decrepit skeleton joins you in this corridor", 30, 430);
    }else if(playerY ==4){
      text ("You think you hear a scream. Just your imagination?", 30, 430);
    }else if (playerY ==5) {
      text ("You're feeling claustrophobic", 30, 430);
    }else if (playerY ==6) {
      text ("A mouse rushes past in the same direction as you", 30, 430);
    }else if (playerY ==7){
     text ("A shield has been shattered like it was glass.", 30, 430); 
    }
  }
  
  //Displays text for the X value 3, in relation to the Y co-ordinates
  if (playerX == 3) {
    if (playerY == 1){
      text ("Your footsteps echo louder here for some reason", 30, 430);
    }
     if (playerY ==2) {
      text ("Maybe you should make a map?", 30, 430);
    }else if (playerY ==3) {
      text ("Everything is quieter here for some reason", 30, 430);
    }else if(playerY ==4){
      text ("The Treasure! Finally! Now GET OUT!", 30, 430);
    }else if (playerY ==5) {
      text ("A large rodent is at the end of the corridor, staring.", 30, 430);
    }else if (playerY ==6) {
      text ("The walls are stained with ancient blood", 30, 430);
    }else if (playerY == 7){
      text ("A wall is falling apart, yet is as solid as ever.", 30, 430);
    }
}

//Displays text for the X value 4, in relation to the Y co-ordinates
if (playerX == 4) {
    if (playerY ==1) {
      if (treasureGet == false){
        text ("The outside light shines through. ", 30, 430);
      }
      else if (treasureGet == true){
        playerWin = true;
      }
    } else if (playerY ==2) {
        text ("Make sure you keep track of your progress", 30, 430);
    }else if (playerY ==3) {
        text ("Hope for the first time in... forever.", 30, 430);
    }else if(playerY ==4){
      text ("You've got a good feeling about this.", 30, 430);
    }else if (playerY ==5) {
      text ("Is this the right way?", 30, 430);
    }else if (playerY ==6) {
      text ("A map. Destroyed and incomprehensible.", 30, 430);
    }else if (playerY == 7){
      text ("There's something that used to be food.", 30, 430);
    }
  }
  
  //Displays text for the X value 5, in relation to the Y co-ordinates
  if (playerX == 5) {
    if (playerY ==1) {
      text ("Should have brought a torch...", 30, 430);
    } else if (playerY ==2) {
     text ("You should write a note...", 30, 430);
    }else if (playerY ==3) {
     text ("Who are you, really?", 30, 430);
    }else if(playerY ==4){
      text ("Everything looks the same", 30, 430);
    }else if (playerY ==5) {
      text ("Maybe just wait for him to take you", 30, 430);
    }else if (playerY ==6) {
      text ("Greed never got anyone anywhere. This proves it.", 30, 430);
    }else if (playerY == 7){
      text ("Here, forever.", 30, 430);
    }
  }
  
  //Displays text for the X value 6, in relation to the Y co-ordinates
  if (playerX == 6) {
    if (playerY ==1) {
      text ("There are fates worse than death. This is one.", 30, 430);
    } else if (playerY ==2) {
      text ("Madness. It's coming", 30, 430);
    }else if (playerY ==3) {
      text ("Was this treasure even worth it?", 30, 430);
    }else if(playerY ==4){
      text ("Why even come here?", 30, 430);
    }else if (playerY ==5) {
      text ("Is this where you'll die?", 30, 430);
    }else if (playerY ==6) {
      text ("Is this it...?", 30, 430);
    }else if (playerY == 7){
      text ("You'll never get out.", 30, 430);
    }
  }
  
  //Displays text for the X value 7, in relation to the Y co-ordinates
  if (playerX == 7) {
    if (playerY == 1){
     text ("He's here!... or not. Just your imagination", 30, 430); 
    }
     if (playerY ==2) {
     text ("Lost.", 30, 430);
    }else if (playerY ==3) {
      text ("Just go back.", 30, 430);
    }else if(playerY ==4){
     text ("Maybe this isn't worth it.", 30, 430);
    }else if (playerY ==5) {
      text ("Where's the exit...", 30, 430);
    }else if (playerY ==6) {
      text ("Hope you kissed your mother goodbye.", 30, 430);
    }else if (playerY ==7){
     text ("All is lost.", 30, 430); 
    }
  }
  }
