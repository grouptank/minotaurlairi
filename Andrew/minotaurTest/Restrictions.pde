//load a different screen image dependant on the user's x and y co-ordinate.
//this first checks the playerX value, then checks the playerY value, and then
//loads an image to screenImage dependant on those values.
//on d, playerX--
//on w, playerY--
//on s, playerY++
//on a, playerX++
void restrictMoves() {
  fill(255);   
  textFont(bodyFont);
  //textDisplay();


  //ROOMS W/ X VALUE OF 1
  //ROOM (1,2) (Restrict Right movement)
  if (playerX == 1) {
    if (playerY ==2) {
      if (key == 'd') {
        playerX--;
      }
    }

    //ROOM (1,4) (Restrict Up movement)
    else if (playerY ==4) {
      if (key == 'w') {
        playerY--;
      }
    }

    //ROOM (1,5) (Restrict Down movement)
    else if (playerY ==5) {
      if (key == 's') {
        playerY++;
      }
    }  

    //ROOM (1,6) (Restrict Up movement)
    else if (playerY ==6) {
      if (key == 'w') {
        playerY--;
      }
    }  

    //ROOM (1,7)(Restrict Down movement)
    else if (playerY ==7) {
      if (key == 's') {
        playerY++;
      }
    }
  }  


  //ROOMS W/ X VALUE OF 2
  //ROOM (2,1) (Restrict Up movement)
  if (playerX == 2) {
    if (playerY ==1) {
      if (key == 'w') {
        playerY--;
      }
    } 

    //ROOM (2,2) (Restrict Down movement)
    else if (playerY ==2) {
      if (key == 's') {
        playerY++;
      }
      //(Restrict Left movement)
      if (key == 'a') {
        playerX++;
        secretText = true;
      }
      //(Restrict Right movement)
      if (key == 'd') {
        playerX--;
      }
    } 

    //ROOM (2,3) (Restrict Up movement)
    else if (playerY ==3) {
      if (key == 'w') {
        playerY--;
      }
      //(Restrict Right movement)
      if (key == 'd') {
        playerX--;
      }
    } 

    //ROOM (2,4) (Restrict Down movement)
    else if (playerY ==4) {
      if (key == 's') {
        playerY++;
      } 
      //(Restrict Right movement)
      if (key =='d') {
        playerX--;
      }
    } 

    //ROOM (2,5) (Restrict Up movement)
    else if (playerY ==5) {
      if (key == 'w') {
        playerY--;
      } 
      //(Restrict Right movement)
      if (key == 'd') {
        playerX--;
      }
    } 

    //ROOM (2,6) (Restrict Down movement)
    else if (playerY ==6) {
      if (key == 's') {
        playerY++;
      }
    }
  }


  //ROOMS W/ X VALUE OF 3
  //ROOM (3,1) (Restrict Right movement)
  if (playerX == 3) {
    if (playerY ==1) {
      if (key == 'd') {
        playerX--;
      }
    } 

    //ROOM (3,2) (Restrict Up movement)
    else if (playerY ==2) {
      if (key == 'w') {
        playerY--;
      }
      //(Restrict Left movement)
      if (key == 'a') {
        playerX++;
      }
    } 

    //ROOM (3,3) (Restrict Down movement)
    else if (playerY ==3) {
      if (key == 's') {
        playerY++;
      }
      //(Restrict Left movement)
      if (key == 'a') {
        playerX++;
      }
    } 

    //ROOM (3,4) (Restrict Up movement)
    else if (playerY ==4) {
      treasureGet = true;
      if (key == 'w') {
        playerY--;
      }
      //(Restrict Left movement)
      if (key == 'a') {
        playerX++;
        secretText = true;
      }
      //(Restrict Right movement)
      if (key=='d') {
        playerX--;
      }
    } 

    //ROOM (3,5) (Restrict Left movement)
    else if (playerY ==5) {
      if (key == 'a') {
        playerX++;
        secretText = true;
      }
      //(Restrict Down movement)
      if (key == 's') {
        playerY++;
      }
      //(Restrict Right movement)
      if (key == 'd') {
        playerX--;
      }
    } 

    //ROOM (3,6) (Restrict Up movement)
    else if (playerY ==6) {
      if (key == 'w') {
        playerY--;
      }
      //(Restrict Right movement)
      if (key == 'd') {
        playerX--;
      }
    } 

    //ROOM (3,7) (Restrict Down movement)
    else if (playerY == 7) {
      if (key == 's') {
        playerY++;
      }
    }
  }


  //ROOMS W/ X VALUE OF 4
  //ROOM (4,1) (Restrict Left movement)
  if (playerX == 4) {
    if (playerY ==1) {
      if (key == 'a') {
        playerX--;
        secretText = true;
      }
      //(Restrict Right movement)
      if (key == 'd') {
        playerX--;
      }
    } 

    //ROOM (4,2) (Restrict Up movement)
    else if (playerY ==2) {
      if (key == 'w') {
        playerY--;
      }
    } 

    //ROOM (4,3) (Restrict Down movement)
    else if (playerY ==3) {
      if (key == 's') {
        playerY++;
      }
      //(Restrict Right movement)
      if (key == 'd') {
        playerX--;
      }
    } 

    //ROOM (4,4) (Restrict Left movement)
    else if (playerY ==4) {
      if (key=='a') {
        playerX++;
        secretText = true;
      }
      //(Restrict Right movement)
      if (key=='d') {
        playerX--;
      }
    } 

    //ROOM (4,5) (Restrict Left movement)
    else if (playerY ==5) {
      if (key == 'a') {
        playerX++;
        secretText = true;
      }
      //(Restroct Right movement)
      if (key == 'd') {
        playerX--;
      }
    } 

    //ROOM (4,6) (Restrict Left movement)
    else if (playerY ==6) {
      if (key == 'a') {
        playerX++;
      }
    } 

    //ROOM (4,7) (Restrict Right movement)
    else if (playerY == 7) {
      if (key == 'd') {
        playerX--;
      }
    }
  }


  //ROOMS W/ X VALUE OF 5
  //ROOM (5,1) (Restrict Left movement)
  if (playerX == 5) {
    if (playerY ==1) {
      if (key == 'a') {
        playerX++;
      }
    } 

    //ROOM (5,2) (Restrict Right movement)
    else if (playerY ==2) {
      if (key == 'd') {
        playerX--;
      }
    } 

    //ROOM (5,3) (Restrict Up movement)
    else if (playerY ==3) {
      if (key == 'w') {
        playerY--;
      }
      //(Restrict Right movement)
      if (key == 'd') {
        playerX--;
      }
      //(Restrict Left movement)
      if (key == 'a') {
        playerX++;
        secretText = true;
      }
    } 

    //ROOM (5,4) (Restrict Left movement)
    else if (playerY ==4) {
      if (key=='a') {
        playerX++;
      }
      //(Restrict Down movement)
      if (key=='s') {
        playerY++;
      }
      //(Restrict  Right movement)
      if (key=='d') {
        playerX--;
      }
    } 

    //ROOM (5,5) (Restrict Left movement)
    else if (playerY ==5) {
      if (key == 'a') {
        playerX++;
        secretText = true;
      }
      //(Restrict Up movement)
      if (key == 'w') {
        playerY--;
      }
    } 

    //ROOM (5,6) (Restrict Right movement)
    else if (playerY ==6) {
      if (key == 'd') {
        playerX--;
      }
      //(Restrict Down movement)
      if (key == 's') {
        playerY++;
      }
    } 

    //ROOM (5,7) (Restrict Left movement)
    else if (playerY == 7) {
      if (key == 'a') {
        playerX++;
      }
    }
  }


  //ROOMS W/ X VALUE OF 6
  //ROOM (6,1) (Restrict Up movement)
  if (playerX == 6) {
    if (playerY ==1) {
      if (key == 'w') {
        playerY--;
      }
      //(Restrict Down movement)
      if (key == 's') {
        playerY++;
      }
    }
    //ROOM (6,2) (Restrict Up movement)
    else if (playerY ==2) {
      if (key == 'w') {
        playerY--;
      }
      //(Restrict Down movement)
      if (key == 's') {
        playerY++;
      }
      //(Restrict Left movement)
      if (key == 'a') {
        playerX++;
      }

      //ROOM (6,3) (Restrict Up movement)
    } else if (playerY ==3) {
      if (key == 'w') {
        playerY--;
      }
      //(Restrict Down movement)
      if (key == 's') {
        playerY++;
      }
      //(Restrict Left movement)
      if (key == 'a') {
        playerX++;
      }
    } 
    
    //ROOM (6,4) (Restrict Down movement)
    else if (playerY ==4) {
      if (key=='s') {
        playerY++;
      }
      //(Restrict Right movement)
      if (key=='d') {
        playerX--;
      }
    } 
    
    //ROOM (6,5)
    else if (playerY ==5) {
    } 
    
    //ROOM (6,6) (Restrict Left movement)
    else if (playerY ==6) {
      if (key == 'a') {
        playerX++;
      } 
      //(Restrict Up movement)
      if (key == 'w') {
        playerY--;
      }
    } 
    
    //ROOM (6,7) (Restrict Down movement)
    else if (playerY == 7) {
      if (key == 's') {
        playerY++;
      }
    }
  }
  
  
  //ROOMS W/ X VALUE OF 7
  //ROOM (7,2) (Restrict Up movement
  if (playerX == 7) {
    if (playerY ==2) {
      if (key == 'w') {
        playerY--;
      }
    } 
    
    //ROOM (7,3) (Restrict Down movement)
    else if (playerY ==3) {
      if (key == 's') {
        playerY++;
      }
    } 
    
    //ROOM (7,4) (Restrict Left movement)
    else if (playerY ==4) {
      if (key=='a') {
        playerX++;
      }
    } 
    
    //ROOM (7,5) (Restrict Up movement)
    else if (playerY ==5) {
      if (key == 'w') {
        playerY--;
      }
    } 
    
    //ROOM (7,6) (Restrict Down movement)
    else if (playerY ==6) {
      if (key == 's') {
        playerY++;
      }
    }
  }
}
