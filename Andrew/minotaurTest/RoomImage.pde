
//load a different screen image dependant on the user's x and y co-ordinate.
//this first checks the playerX value, then checks the playerY value, and then
//loads an image to screenImage dependant on those values.
void loadScreen() {
  //Loads in images for the X value 1, in relation to the Y co-ordinates
  if (playerX == 1) {
    if (playerY ==1) {
      screenImage = loadImage("images/map_G1.png");
    } else if (playerY ==2) {
      screenImage = loadImage("images/map_F1.png");
    } else if (playerY ==3) {
      screenImage = loadImage("images/map_E1.png");
    } else if (playerY ==4) {
      screenImage = loadImage("images/map_D1.png");
    } else if (playerY ==5) {
      screenImage = loadImage("images/map_C1.png");
    } else if (playerY ==6) {
      screenImage = loadImage("images/map_B1.png");
    } else if (playerY ==7) {
      screenImage = loadImage("images/map_A1.png");
    }
  } 
  
  //Loads in images for the X value 2, in relation to the Y co-ordinates
  else if (playerX == 2) {
    if (playerY ==1) {
      screenImage = loadImage("images/map_G2.png");
    } else if (playerY ==2) {
      screenImage = loadImage("images/map_F2.png");
    } else if (playerY ==3) {
      screenImage = loadImage("images/map_E2.png");
    } else if (playerY ==4) {
      screenImage = loadImage("images/map_D2.png");
    } else if (playerY ==5) {
      screenImage = loadImage("images/map_C2.png");
    } else if (playerY ==6) {
      screenImage = loadImage("images/map_B2.png");
    } else if (playerY ==7) {
      screenImage = loadImage("images/map_A2.png");
    }
  } 
  
  //Loads in images for the X value 3, in relation to the Y co-ordinates
  else if (playerX == 3) {
    if (playerY ==1) {
      screenImage = loadImage("images/map_G3.png");
    } else if (playerY ==2) {
      screenImage = loadImage("images/map_F3.png");
    } else if (playerY ==3) {
      screenImage = loadImage("images/map_E3.png");
    } else if (playerY ==4) {
      screenImage = loadImage("images/map_D3_TREASURE.png");
    } else if (playerY ==5) {
      screenImage = loadImage("images/map_C3.png");
    } else if (playerY ==6) {
      screenImage = loadImage("images/map_B3.png");
    } else if (playerY ==7) {
      screenImage = loadImage("images/map_A3.png");
    }
  } 
  
  //Loads in images for the X value 4, in relation to the Y co-ordinates
  else if (playerX == 4) {
    if (playerY ==1) {
      screenImage = loadImage("images/map_G4_ENTRANCE.png");
    } else if (playerY ==2) {
      screenImage = loadImage("images/map_F4.png");
    } else if (playerY ==3) {
      screenImage = loadImage("images/map_E4.png");
    } else if (playerY ==4) {
      screenImage = loadImage("images/map_D4.png");
    } else if (playerY ==5) {
      screenImage = loadImage("images/map_C4.png");
    } else if (playerY ==6) {
      screenImage = loadImage("images/map_B4.png");
    } else if (playerY ==7) {
      screenImage = loadImage("images/map_A4.png");
    }
  } 
  
  //Loads in images for the X value 5, in relation to the Y co-ordinates
  else if (playerX == 5) {
    if (playerY ==1) {
      screenImage = loadImage("images/map_G5.png");
    } else if (playerY ==2) {
      screenImage = loadImage("images/map_F5.png");
    } else if (playerY ==3) {
      screenImage = loadImage("images/map_E5.png");
    } else if (playerY ==4) {
      screenImage = loadImage("images/map_D5.png");
    } else if (playerY ==5) {
      screenImage = loadImage("images/map_C5.png");
    } else if (playerY ==6) {
      screenImage = loadImage("images/map_B5.png");
    } else if (playerY ==7) {
      screenImage = loadImage("images/map_A5.png");
    }
  } 
  
  //Loads in images for the X value 6, in relation to the Y co-ordinates
  else if (playerX == 6) {
    if (playerY ==1) {
      screenImage = loadImage("images/map_G6.png");
    } else if (playerY ==2) {
      screenImage = loadImage("images/map_F6.png");
    } else if (playerY ==3) {
      screenImage = loadImage("images/map_E6.png");
    } else if (playerY ==4) {
      screenImage = loadImage("images/map_D6.png");
    } else if (playerY ==5) {
      screenImage = loadImage("images/map_C6.png");
    } else if (playerY ==6) {
      screenImage = loadImage("images/map_B6.png");
    } else if (playerY ==7) {
      screenImage = loadImage("images/map_A6.png");
    }
  } 
  
  //Loads in images for the X value 7, in relation to the Y co-ordinates
  else if (playerX == 7) {
    if (playerY ==1) {
      screenImage = loadImage("images/map_G7.png");
    } else if (playerY ==2) {
      screenImage = loadImage("images/map_F7.png");
    } else if (playerY ==3) {
      screenImage = loadImage("images/map_E7.png");
    } else if (playerY ==4) {
      screenImage = loadImage("images/map_D7.png");
    } else if (playerY ==5) {
      screenImage = loadImage("images/map_C7.png");
    } else if (playerY ==6) {
      screenImage = loadImage("images/map_B7.png");
    } else if (playerY ==7) {
      screenImage = loadImage("images/map_A7.png");
    }
  }
}
//this is the game over screen
void gameOver(){
 background(0);
 textSize (30);
    textAlign (CENTER);
    text ("YOU DIED", width/2, 200);
}
//this is the win screen
void winScreen(){
   background(0);
 textSize (30);
    textAlign (CENTER);
    text ("YOU WIN!", width/2, 200);
}
