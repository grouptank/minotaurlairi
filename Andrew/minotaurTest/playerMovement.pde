void keyPressed() {
  secretText = false;
  restrictMoves();
  //textDisplay();
  //check if the player is alive; if so, let them walk
  if (playerMove == true) {
    //player moves up when they press w. It also resets music-trigger variables,
    //stops all music so it can be reset and played again without overlap, and
    //plays the walk sound effect.


    if (key == 'w') {
      if (playerY < 7) {
        playerY++;
      }
      roundNum++;
      totalroundNum++;
      walk.rewind();
      walk.play();
      playDeath=0;
      playDistant=0;
      playMid=0;
      playClose=0;
      distant.pause();
      midWay.pause();
      close.pause();
      MFar.pause();
      MNear.pause();
      MClose.pause();
      fire.pause();
      //player moves down when they press s. It also resets music-trigger variables,
      //stops all music so it can be reset and played again without overlap, and
      //plays the walk sound effect.
    }


    if (key == 's') {
      if (playerY > 1)
        playerY--;
      roundNum++;
      totalroundNum++;
      walk.rewind();
      walk.play();
      playDeath=0;
      playDistant=0;
      playMid=0;
      playClose=0;
      distant.pause();
      midWay.pause();
      close.pause();
      MFar.pause();
      MNear.pause();
      MClose.pause();
      fire.pause();
      //player moves left when they press a. It also resets music-trigger variables,
      //stops all music so it can be reset and played again without overlap, and
      //plays the walk sound effect.
    }


    if (key == 'a') {
      if (playerX > 1)
        playerX--;
      roundNum++;
      totalroundNum++;
      walk.rewind();
      walk.play();
      playDeath=0;
      playDistant=0;
      playMid=0;
      playClose=0;
      distant.pause();
      midWay.pause();
      close.pause();
      MFar.pause();
      MNear.pause();
      MClose.pause();
      fire.pause();
      //player moves right when they press d. It also resets music-trigger variables,
      //stops all music so it can be reset and played again without overlap, and
      //plays the walk sound effect.
    }


    if (key == 'd') {
      if (playerX < 7)
        playerX++;
      roundNum++;
      totalroundNum++;
      walk.rewind();
      walk.play();
      playDeath=0;
      playDistant=0;
      playMid=0;
      playClose=0;
      distant.pause();
      midWay.pause();
      close.pause();
      MFar.pause();
      MNear.pause();
      MClose.pause();
      fire.pause();
      //re-randomize the minotaur's position; for debugging
    }
  } else {
  }
  if (playerLife == false) {
    distant.pause();
    midWay.pause();
    close.pause();
    fire.pause();
    MFar.pause();
    MNear.pause();
    MClose.pause();
  }
}
