class Door {

  //Sets Start button image for player to press
  void startButton() {
    fill (255, 0, 0);
    rectMode (CENTER);
    rect (300, 200, 100, 60);
    fill (255);
    textSize (30);
    textAlign (CENTER);
    text ("ENTER", width/2, 200);

    //Starts game if the mouse is pressed within the confines of the door
    if (mousePressed) { 
      if (mouseX>156 && mouseX<345 && mouseY>83 && mouseY<342)
        click = true;
      timePassed = millis();
      door.play();
    } //else {
    //click = false;
    //}
  }

  //Closed door of start menu
  void doorClosed() {
    screenImage = loadImage("images/door_closed.png");
    noStroke();
    rectMode (CENTER);
    fill (150);
    rect (width/2, height/2, 150, 300);
    stroke (0);
    fill (20);
    line (300, 150, 300, 450);
    line (0, 450, 600, 450);
  }

  //Starting Animation of door opening
  void doorHalf() {
    screenImage = loadImage("images/door_half.png");
    noStroke();
    fill (150);
    rect (width/2, height/2, 150, 300);
    fill(20);
    rect (width/2, height/2, 80, 300);
    line (0, 450, 600, 450);
  }

  //Door animation displays fully opened door
  void doorOpen() {
    screenImage = loadImage("images/door_open.png");
    fill (20);
    rect (width/2, height/2, 150, 300);
    line (0, 450, 600, 450);
  }

  //Camera zooms into door empty space
  void doorZoom1() {
    screenImage = loadImage("images/door_zoom1.png");
    fill (20);
    rect (width/2, height/2, 250, 500);
    line (0, 550, 600, 550);
  }

  //Camera zooms into door empty space
  void doorZoom2() {
    screenImage = loadImage("images/door_zoom2.png");
    rect (width/2, height/2, 600, 600);
  }
  
  //Camera zooms into door empty space
  void doorZoom3() {
    screenImage = loadImage("images/door_zoom3.png");
    rect (width/2, height/2, 600, 600);
  }
 
  //Camera zooms into door empty space
  void doorZoom4() {
    screenImage = loadImage("images/door_zoom3.png");
    rect (width/2, height/2, 600, 600);
    dripping.loop();
  }
}
