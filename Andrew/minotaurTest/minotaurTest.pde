//This code requires the Minim library
import ddf.minim.*;
import ddf.minim.analysis.*;
import ddf.minim.effects.*;
import ddf.minim.signals.*;
import ddf.minim.spi.*;
import ddf.minim.ugens.*;

import ddf.minim.*;
Minim minim;

//Declare our Minim Audio variables
AudioPlayer walk;        //Player's Walking Sound
AudioPlayer death;       //Player's Death Sound
AudioPlayer distant;     //Ambient Distant Music
AudioPlayer midWay;      //Ambient Midway Music
AudioPlayer close;       //Ambient Close Music
AudioPlayer MFar;        //Minotaur's Far Sounds
AudioPlayer MNear;       //Minotaur's Near Sounds
AudioPlayer MClose;      //Minotaur's Close Sounds
AudioPlayer dripping;    //Ambient Dripping Sound
AudioPlayer fire;        //Torch Fire Sound
AudioPlayer door;        //DOOOR
AudioPlayer victory;     //Victory noise

//Declare Door class for start screen
Door startScreen;

//start screen variables
int timePassed;
boolean click;
boolean startGame;

//player position variables
int playerX = 4;
int playerY = 1;

//minotaur position variables
int minotaurX = 0;
int minotaurY = 0;

//variables for tracking the minotaur
int minotaurDistanceX;
int minotaurDistanceY;
int minotaurDistanceAvg;
boolean panicMode = false;

//variables for triggering music
int playDeath = 0;
int playDistant = 0;
int playMid = 0;
int playClose = 0;

//variable for player's life status
boolean playerMove = true;
boolean playerLife = true;
boolean secretText = false;
boolean treasureGet = false;
boolean playerWin = false; 

//variable for the image displayed on screen
PImage screenImage;

//declare our font variables
PFont bodyFont;
PFont messageFont;

//variables for rounds
int roundNum=0;
int totalroundNum=0;

void setup() {
  //set the size of the window
  size (500, 500);
  minim = new Minim(this);

  //calling on Door class
  startScreen = new Door();

  //load our sound files
  minim = new Minim(this);
  walk = minim.loadFile("Audio/walking.wav");
  death = minim.loadFile("Audio/death.wav");
  distant = minim.loadFile("Audio/Distant Sound.wav");
  midWay = minim.loadFile("Audio/Getting Closer.wav");
  close = minim.loadFile("Audio/Near You.wav");
  door = minim.loadFile("Audio/door.wav");
  MFar = minim.loadFile("Audio/M Far Noise.mp3");
  MNear = minim.loadFile("Audio/M Near Noise.mp3");
  MClose = minim.loadFile("Audio/M Close Noise.mp3");
  dripping = minim.loadFile("Audio/Dripping.mp3");
  fire = minim.loadFile("Audio/Torch Fire.mp3");
  victory = minim.loadFile("Audio/Victory.wav");

  //Create our fonts
  bodyFont = createFont("Veranda", 18);
  messageFont = createFont("Veranda", 14);
}

void draw() {

  //--//create our start screen//--//

  //set background
  background(0);

  //call functions from Door class
  startScreen.doorClosed();
  startScreen.startButton();  

  //animation... of the door.
  doorAnimation();

  //load in screenImage (value is determined in roomImage)
  image(screenImage, 0, 0, width, height);

  //--//Run our actual game//--//

  if (startGame) {

    //load in screenImage (value is determined in roomImage)
    image(screenImage, 0, 0, width, height);

    //call our functions
    loadScreen();
    minotaurUpdate();
    minotaurDetection();
    playMusic();

    //load in screenImage (value is determined in roomImage)
    image(screenImage, 0, 0, width, height);

    //create our text box
    rectMode(CENTER);
    stroke(255);
    strokeWeight(4);
    fill(0);
    rect (250, 400, 496, 150);

    //load in our main body text
    textAlign (LEFT);
    textDisplay();

    //load in text for additional messages
    textFont(messageFont);
    if (secretText == true)
      text ("You found a secret passage!", 50, 350);
    if (treasureGet == true)
      text ("Treasure:OBTAINED", 350, 350);

    //load our game over screen
    if (playerLife == false) {
      gameOver();
      distant.pause();
      midWay.pause();
      close.pause();
    }
    //load our win screen
    if (playerWin == true) {
      winScreen();
      victory.play();
      playerMove=false;
      distant.pause();
      midWay.pause();
      close.pause();
      MFar.pause();
      MNear.pause();
      MClose.pause();
      fire.pause();
    }
  }
}

//uses a series of timers (using millis) to play an animation of the door opening.
void doorAnimation() {

  //load in screenImage (value is determined in roomImage)
  image(screenImage, 0, 0, width, height);

  //use a series of timers to create an animation of a door opening
  if (click && millis() - timePassed > 1000) {
    startScreen.doorHalf();
  }

  if (click && millis() - timePassed >2000) {
    startScreen.doorOpen();
  }

  if (click && millis() - timePassed > 3000) {
    startScreen.doorZoom1();
  }

  if (click && millis() - timePassed > 4000) {
    startScreen.doorZoom2();
  }
  if (click && millis() - timePassed > 5000) {
    startScreen.doorZoom3();
  }
  if (click && millis() - timePassed > 6000) {
    startScreen.doorZoom4();

    //set the startGame boolean to true, so our game runs
    startGame = true;
  }
}
