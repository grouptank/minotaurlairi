void minotaurDetection() {
  fill(255);   
  textFont(bodyFont);
  //find out how far the minotaur is from the player; both along the
  //x and y axis, and the average between the two
  minotaurDistanceX = playerX - minotaurX;
  minotaurDistanceY = playerY - minotaurY;
  minotaurDistanceAvg = minotaurDistanceX + minotaurDistanceY;

  //kill the player if the player's x and y is the same as the minotaur's
  if (minotaurX == playerX) {
    if (minotaurY == playerY) {
      //put the player into the "death state"
      playerLife=false;
      playerMove=false;
      playDeath++;
      //stop the music
      distant.pause();
      midWay.pause();
      close.pause();
    }
  }
  
  //if the player is within 3 spaces of the minotaur, play one piece of music
  if (minotaurDistanceX <=3 && minotaurDistanceX >= -3) {
    if (minotaurDistanceY <=3 && minotaurDistanceY >= -3) {
      if (minotaurDistanceAvg <=3 && minotaurDistanceAvg >=-3) {
        //increase the playDistant variable to trigger the if in playSound
        playDistant++;
        text ("a fair distance", 50, 450);
      }
    }
  }
  
  //if the player is within 2 spaces of the minotaur, play creepy sound effects
  if (minotaurDistanceX <=2 && minotaurDistanceX >= -2) {
    if (minotaurDistanceY <=2 && minotaurDistanceY >= -2) {
      if (minotaurDistanceAvg <=2 && minotaurDistanceAvg >=-2) {
        //increase the playMid variable to trigger the if in playSound
        playMid++;
        text ("getting there", 50, 460);
      }
    }
  }
  
  //if the player is within 1 space of the minotaur, build on the music
  if (minotaurDistanceX <=1 && minotaurDistanceX >= -1) {
    if (minotaurDistanceY <=1 && minotaurDistanceY >= -1) {
      if (minotaurDistanceAvg <=1 && minotaurDistanceAvg >=-1) {
        //increase the playClose variable to trigger the if in playSound
        playClose++;
        text ("you're gonna die", 50, 470);
      }
    }
  }
}


//we need a separate function to play the music in; otherwise, it keeps playing
//new instances of the same piece of music. Doing it this way means we can tie each
//one to a variable, and then it only plays when that variable reaches a value of one;
//and so it only plays it once.

void playMusic() {
  if (playDistant==1) {
    distant.rewind();
    distant.play();
    MFar.rewind();
    MFar.play();
  } 
  if (playDeath==1) {
    death.play();
  } 
  if (playMid==1) {
    midWay.rewind();
    midWay.play();
    MNear.rewind();
    MNear.play();
  } 
  if (playClose ==1) {
    close.rewind();
    close.play();
    MClose.rewind();
    MClose.play();
  }
}
